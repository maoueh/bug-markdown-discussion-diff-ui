# Bug Description

It seems there is a bug with the diff show in a merge request
discussion for markdown content. Indeed, when someone comment
on a modified markdown file, each line in the diff is doubled
compared to what it should be be.

Here an example:

I prepared a repo on gitlab to reproduce this:



